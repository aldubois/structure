Welcome to Structure's documentation!
=====================================


User Manual
-----------

Installation
************

Clone the repository.

.. code-block:: bash
		
		git clone https://gitlab.com/aldubois/recoprot.git


Setup and enter your virtual environment.

.. code-block:: bash
		
		VIRTUALENV=/path/to/your/future/virtual/env
		mkdir ${VIRTUALENV}
		python3 -m venv ${VIRTUALENV}
		source ${VIRTUALENV}/bin/activate



Install the prerequisites.

.. code-block:: bash
		
		cd recoprot
		make init



Launch test base.

.. code-block:: bash

		make test



Proceed with installation.

.. code-block:: bash

		make install


Build and open the documentation.
   
.. code-block:: bash
		
		make doc
		firefox doc/_build/html/index.html &




Library Documentation
---------------------

.. automodule:: structure.__init__
    :members:


Tests Documentation
-------------------

.. automodule:: tests.context_test
    :members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
