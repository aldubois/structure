# -*- coding: utf-8 -*-

"""
Adapt the import to point to the source code.
"""

import sys
import os

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import structure
