# -*- coding: utf-8 -*-

"""
Context tests
=============

"""

import os


def test_context():
    """
    Verify library import.

    Raises
    ------
    ImportError
       The library import failed.
    AssertionError
       The imported library is not the source code in this repository.
    """
    from .context import structure
    this_dir = os.path.dirname(__file__)
    lib_path = os.path.join(this_dir, "..", "structure", "__init__.py")
    ref = os.path.abspath(lib_path)
    assert ref == structure.__file__
