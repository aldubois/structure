default: help

## Install library requirements
init:
	pip3 install -r requirements.txt

## Run tests
test:
	(pip3 install -r tests/requirements.txt; pytest tests)

## Install library
install:
	python3 setup.py install

## Build html documentation
.PHONY : doc
doc:
	(cd doc; pip3 install -r requirements.txt; make html; cd ..)

## Clean the repository
clean:
	(python3 setup.py clean --all; rm -rf dist structure.egg-info)

## Full installation pipeline
all: init test install doc clean

## This help screen
help:
	@printf "Available targets:\n\n"
	@awk '/^[a-zA-Z\-\_0-9%:\\]+/ { \
          helpMessage = match(lastLine, /^## (.*)/); \
          if (helpMessage) { \
            helpCommand = $$1; \
            helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
      gsub("\\\\", "", helpCommand); \
      gsub(":+$$", "", helpCommand); \
            printf "  \x1b[32;01m%-35s\x1b[0m %s\n", helpCommand, helpMessage; \
          } \
        } \
        { lastLine = $$0 }' $(MAKEFILE_LIST) | sort -u
	@printf "\n"
