# -*- coding: utf-8 -*-

# Learn more: https://github.com/kennethreitz/setup.py

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='structure',
    version='0.1.0',
    description='Template package',
    long_description=readme,
    author='Alexandre Dubois',
    author_email='alexandre.dubois@zaclys.net',
    url='https://gitlab.com/aldubois/structure.git',
    license=license,
    packages=find_packages(exclude=('tests', 'docs', 'env')),
    entry_points={
        'console_scripts': [
            'structure = structure:main',
        ],
    }
)
